<?php
    
    require_once("lib/nusoap.php");
    require_once("vendor/autoload.php");

    use Rain\Tpl;
    // config
    $configTpl = array(
        "tpl_dir"       => "tpl/",
        "cache_dir"     => "cache/"
    );
    Tpl::configure( $configTpl );

    function utf8_encode_deep(&$input) {
        if (is_string($input)) {
            $input = utf8_encode($input);
        } else if (is_array($input)) {
            foreach ($input as &$value) {
                utf8_encode_deep($value);
            }

            unset($value);
        } else if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));

            foreach ($vars as $var) {
                utf8_encode_deep($input->$var);
            }
        }
    }

//nusoap
    if(!isset($_POST["codigoRastro"])){
        header("Location: index.php");
    }

    $objetos = $_POST["codigoRastro"];
    $client = new nusoap_client("http://webservice.correios.com.br/service/rastro/Rastro.wsdl", 'wsdl');
    
    $parametros = array('usuario' => 'ECT',
                        'senha' => 'SRO',
                        'tipo' => 'U',
                        'resultado' => 'T',
                        'lingua' => '101',
                        'objetos' => $objetos
                    );
    
    $resultado = $client->call('buscaEventos',$parametros);
    utf8_encode_deep($resultado['return']);

    $situacao;
    if($resultado['return']['objeto']['evento']['tipo'] == "PO"){
        $situacao = "Sua encomenda foi postada!";
    }
    elseif($resultado['return']['objeto']['evento']['tipo'] == "DO"){
        $situacao = "Sua encomenda está a caminho!";
    }
    elseif($resultado['return']['objeto']['evento']['tipo'] == "OEC" && $resultado['return']['objeto']['evento']['status'] == "01"){
        $situacao = "Sua encomenda saiu para entrega ao destinatário!";
    }
    elseif(($resultado['return']['objeto']['evento']['tipo'] == "BDE" || $resultado['return']['objeto']['evento']['tipo'] == "BDI") && $resultado['return']['objeto']['evento']['status'] == "01"){
        $situacao = "Sua encomenda já foi entregue!";
    }
    else{
        $situacao = "Ocorreu um erro na sua encomenda!";
    }

//TPL
    $tpl = new Tpl;

    // assign a variable
    $tpl->assign( "situacao", $situacao);
    $tpl->assign( "numero", $resultado['return']['objeto']['numero'] );
    $tpl->assign( "data", $resultado['return']['objeto']['evento']['data'] );
    $tpl->assign( "hora", $resultado['return']['objeto']['evento']['hora'] );
    $tpl->assign( "cidade", $resultado['return']['objeto']['evento']['cidade'] );
    $tpl->assign( "uf", $resultado['return']['objeto']['evento']['uf'] );
    $tpl->assign( "descricao", $resultado['return']['objeto']['evento']['descricao'] );
    $tpl->assign( "detalhe", $resultado['return']['objeto']['evento']['detalhe'] );

    // draw the template
    $html = $tpl->draw("rastreamento", true);

    file_put_contents('hmtlRastro.html', $html);

    echo $html;

//Dompdf
    use Dompdf\Dompdf;

    // instanciando o dompdf
    $dompdf = new Dompdf();

    //inserindo o HTML que queremos converter
    $dompdf->loadHtml($html);

    // Definindo o papel e a orientação
    $dompdf->setPaper('A4', 'landscape');

    // Renderizando o HTML como PDF
    $dompdf->render();

    // Salvando o PDF no servidor
    file_put_contents("Rastreamento.pdf", $dompdf->output());

//form para o email
    echo "Digite o endereço de e-mail:";
    echo "<form action=\"email.php\" method=\"POST\">";
        echo "<input type=\"email\" name=\"emailAdd\"/>";
        echo "<input type=\"hidden\" name=\"assunto\" value=\"$situacao\"/>";
        echo "<button class=\"submit\">Enviar por e-mail</button>";
    echo "</form>";

//OA016913717BR OL430063892BR
?>