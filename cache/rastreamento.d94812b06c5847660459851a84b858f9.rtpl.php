<?php if(!class_exists('Rain\Tpl')){exit;}?><!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Rastreamento do objeto <?php echo htmlspecialchars( $numero, ENT_COMPAT, 'UTF-8', FALSE ); ?></title>
    <style type="text/css">
        
        html{
            margin: 0;
            padding: 0;
        }

        h1{
            color: #3b87ac;
        }
        h3{
            margin-bottom: 0;
            padding-bottom: 0;
        }

        .titulo{
            margin: 20px 0px 20px 0px;
        }

        .container{
            clear: both;
            border-top: dotted;
            width: 600px;

        }

        .info{

            float: left;
            margin: 10px;

        }

        .desc{

            float: left;
            margin: 10px;

        }

        .dados{

            clear: both;

        }

    </style>
</head>
<body>
    <h1><?php echo htmlspecialchars( $situacao, ENT_COMPAT, 'UTF-8', FALSE ); ?></h1>
    <div class="titulo">
        Você pode acompanhar o envio com o código de rastreamento <a href="https://www2.correios.com.br/sistemas/rastreamento/default.cfm"><?php echo htmlspecialchars( $numero, ENT_COMPAT, 'UTF-8', FALSE ); ?></a>
    </div>
    <div class="container">
        <div class="info"> 

            <?php echo htmlspecialchars( $data, ENT_COMPAT, 'UTF-8', FALSE ); ?><br/>
            <?php echo htmlspecialchars( $hora, ENT_COMPAT, 'UTF-8', FALSE ); ?><br/>
            <?php echo htmlspecialchars( $cidade, ENT_COMPAT, 'UTF-8', FALSE ); ?> / <?php echo htmlspecialchars( $uf, ENT_COMPAT, 'UTF-8', FALSE ); ?><br/>

        </div>
        <div class="desc">
            
            <strong><?php echo htmlspecialchars( $descricao, ENT_COMPAT, 'UTF-8', FALSE ); ?><br/></strong>
            <?php echo htmlspecialchars( $detalhe, ENT_COMPAT, 'UTF-8', FALSE ); ?><br/>

        </div>
    </div>
    <div class="dados">
        <h3>Dados do envio</h3><br/>
        Nome: Pedro Augusto Tolentino Pereira<br/>
        Email: sryakuza@gmail.com<br/>
        Gitlab: https://gitlab.com/sryakuza/<br/>
    </div>
    <br/>
</body>
</html>