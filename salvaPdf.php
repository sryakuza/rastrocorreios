<?php    

    require_once("vendor/autoload.php");

    use Dompdf\Dompdf;

    // instanciando o dompdf
    $dompdf = new Dompdf();

    //lendo o arquivo HTML correspondente
    $html1 = file_get_contents("hmtlRastro.html");

    //inserindo o HTML que queremos converter
    $dompdf->loadHtml($html1);

    // Definindo o papel e a orientação
    $dompdf->setPaper('A4', 'landscape');

    // Renderizando o HTML como PDF
    $dompdf->render();

    // Salvando o PDF no servidor
    file_put_contents("Rastreamento.pdf", $dompdf->output());

 ?>